<?php
/*
 *
 * Copyright (C) SPREAD WORKS Inc. All Rights Reserved.
 *
 */

namespace Customize\Twig\Extension;

use Doctrine\Common\Collections;
use Doctrine\ORM\EntityManagerInterface;
use Eccube\Common\EccubeConfig;
use Eccube\Entity\Master\ProductStatus;
use Eccube\Entity\Product;
use Eccube\Entity\ProductClass;
use Eccube\Repository\ProductRepository;

class TwigExtension extends \Twig_Extension
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @var EccubeConfig
     */
    protected $eccubeConfig;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * TwigExtension constructor.
     *
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        EccubeConfig $eccubeConfig, 
        ProductRepository $productRepository
    ) {
        $this->entityManager = $entityManager;
        $this->eccubeConfig = $eccubeConfig;
        $this->productRepository = $productRepository;
    }
    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('CustomizeNewProduct', array($this, 'getCustomizeNewProduct')),
            new \Twig_SimpleFunction('CustomizeProductByParamCat', array($this, 'getCustomizeProductByParamCat')),
        );
    }

    /**
     * Name of this extension
     *
     * @return string
     */
    public function getName()
    {
        return 'CustomizeTwigExtension';
    }

    /**
     * 
     * GET NEW PRODUCT
     * 
     * @return Products|null
     */
    public function getCustomizeNewProduct() {
        try {
            // 既存のproductRepositoryを利用し、商品情報を取得

            // 検索条件の新着順を定義
            $searchData = array();
            $qb = $this->entityManager->createQueryBuilder();
            $query = $qb->select("plob")
                ->from("Eccube\\Entity\\Master\\ProductListOrderBy", "plob")
                ->where('plob.id = :id')
                ->setParameter('id', $this->eccubeConfig['eccube_product_order_newer'])
                ->getQuery();
            $searchData['orderby'] = $query->getOneOrNullResult();

            // 新着順の商品情報3件取得
            $qb = $this->productRepository->getQueryBuilderBySearchData($searchData);
            $query = $qb->setMaxResults(3)->getQuery();
            $products = $query->getResult();
            return $products;

        } catch (\Exception $e) {
            return null;
        }
        return null;
    }


    // GET CUSTOMIZE PRODUCT BY PARAM CATEGORY ID
    public function getCustomizeProductByParamCat( $cat_get_id = 0, $limit = -1 ) {
        try {
            $cat_get_id = isset($cat_get_id) ? intval($cat_get_id) : 0;

            if( $cat_get_id ) {

                $data_result = array();

                // GET PRODUCTS FILTER CATEGORY
                $limit_product = '';
                if( $limit > 0 ) {
                    $limit_product = " LIMIT ".$limit;
                }
                $PRODUCT_CAT_QUERY = "SELECT p.id, p.name, img.file_name 'main_list_image', pc.product_code, pc.price02
                            FROM dtb_product AS p
                            INNER JOIN dtb_product_category AS pcat ON p.id=pcat.product_id 
                            INNER JOIN dtb_category AS cat ON pcat.category_id=cat.id 
                            INNER JOIN dtb_product_class AS pc ON p.id=pc.product_id 
                            LEFT JOIN dtb_product_image AS img ON img.product_id=p.id 
                            WHERE cat.id = ".$cat_get_id."
                            ORDER BY p.create_date DESC" . $limit_product;
                $statement_p = $this->entityManager->getConnection()->prepare($PRODUCT_CAT_QUERY);
                $statement_p->execute();
                $data_products = $statement_p->fetchAll();


                // GET CAT DATA
                $data_result['catData'] = '';
                $CAT_QUERY = "SELECT cat.id, cat.category_name
                            FROM dtb_category AS cat
                            WHERE cat.id = ".$cat_get_id."
                            ORDER BY cat.create_date DESC";
                $statement_c = $this->entityManager->getConnection()->prepare($CAT_QUERY);
                $statement_c->execute();
                $data_cats = $statement_c->fetchAll();
                if( $data_cats && count($data_cats) > 0 ) {
                    $data_result['catData'] = $data_cats[0];
                }

                $data_result['Products'] = $data_products;
                
                return $data_result;

            }

        } catch (\Exception $e) {
            return null;
        }
        return null;
    }

}
