<?php

namespace Customize\Controller\Admin\Blog;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Eccube\Controller\AbstractController;
use Eccube\Entity\BaseInfo;
use Eccube\Repository\BaseInfoRepository;
use Doctrine\ORM\EntityManagerInterface;

class BlogAdminController extends AbstractController
{

	/**
     * @var \Swift_Mailer
     */
    protected $mailer;

	/**
     * @var BaseInfo
     */
    protected $BaseInfo;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;


    public function __construct(
    	\Swift_Mailer $mailer,
    	EntityManagerInterface $entityManager,
        BaseInfoRepository $baseInfoRepository
    ) {
    	$this->mailer = $mailer;
    	$this->entityManager = $entityManager;
        $this->BaseInfo = $baseInfoRepository->get();
    }


    /**
     * @Route("/%eccube_admin_route%/blog", name="admin_blog")
     * @Template("@admin/Blog/index.twig")
     */
    public function blog_index( Request $request ) {

        // DATA RETURN
        $data_result = array(
            'name' => 'BLOG INDEX',
        );
        
        return $data_result;

    }


    /**
     * @Route("/%eccube_admin_route%/blog/detail/{id}", requirements={"id" = "\d+"}, name="admin_blog_detail")
     * @Template("@admin/Blog/detail.twig")
     */
    public function blog_detail( Request $request, $id = null ) {

        // DATA RETURN
        $data_result = array(
            'blog_id' => $id,
            'name' => 'BLOG DETAIL',
        );
        
        return $data_result;

    }


}