<?php

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Archive Sidebar',
		'id'   => 'archive-sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="sbarchive %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Category Sidebar',
		'id'   => 'category-sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="sbcategory %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}


?>

<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Archive Sidebar')) : else : ?>
<?php endif; ?>

<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Category Sidebar')) : else : ?>
<?php endif; ?>

<?php
//Define function
define( 'THEME_URL', get_stylesheet_directory() );
define( 'CORE_DIR_URL', THEME_URL . '/core' );
define( 'FUNC_DIR_URL', THEME_URL . '/functions' );
define( 'TEMP_DIR_URL', THEME_URL . '/templates' );
define( 'THEME_ROOT_URL', get_theme_root_uri() );


// Add Js Admin
function load_custom_wp_admin_script() {
    wp_enqueue_style( 'custom_admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
    wp_enqueue_script( 'custom_admin_js', get_template_directory_uri() . '/js/admin-script.js', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script' );


//Delete width and height in the post thumnail
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


//The Post Thumbnail
<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'alignleft' ) ); ?>
<?php
//Get Thumbnail URL
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$urlthumb = $thumb['0'];
?>


<?php

//POST COUNT
$wp_query->found_posts;


//Get category in cureent cat
echo get_category_link(the_category_ID($echo=false));
$thisCat = get_category(get_query_var('cat'));

//Category Object
stdClass Object
(
    [term_id] => 85
    [name] => Category Name
    [slug] => category-name
    [term_group] => 0
    [term_taxonomy_id] => 85
    [taxonomy] => category
    [description] => 
    [parent] => 70
    [count] => 0
    [cat_ID] => 85
    [category_count] => 0
    [category_description] => 
    [cat_name] => Category Name
    [category_nicename] => category-name
    [category_parent] => 70
)


//WP LIST CATEGORY
$arg = array(
        'child_of'            => 0,
        'current_category'    => 0,
        'depth'               => 0,
        'echo'                => 1,
        'exclude'             => '',
        'exclude_tree'        => '',
        'feed'                => '',
        'feed_image'          => '',
        'feed_type'           => '',
        'hide_empty'          => 1,
        'hide_title_if_empty' => false,
        'hierarchical'        => true,
        'order'               => 'ASC',
        'orderby'             => 'name',
        'separator'           => '<br />',
        'show_count'          => 0,
        'show_option_all'     => '',
        'show_option_none'    => __( 'No categories' ),
        'style'               => 'list',
        'taxonomy'            => 'category',
        'title_li'            => __( 'Categories' ),
        'use_desc_for_title'  => 1,
    );
?>
<ul>
    <?php wp_list_categories( $arg ); ?> 

    <?php wp_list_categories( array(
        'orderby' => 'name',
        'include' => array( 3, 5, 9, 16 )
    ) ); ?> 

</ul>


// ACF
<?php if( get_field('sub_heading') ): ?>
	<h2><?php the_field('sub_heading'); ?></h2>
<?php endif; ?>
<?php
$field_name = "text_field";
$field = get_field_object($field_name);
?>


<?php
// ADD GOOGLE KEY
function my_acf_google_map_api( $api ){
    
    $api['key'] = 'AIzaSyBCX8kPzEC8cn-12QGals7k2nVa1JS_gx8';
    
    return $api;

}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
?>



//MENU
<?php wp_nav_menu(array('menu' => 'mainMenu', 'container' => '', 'menu_class' => 'menu')); ?>

<?php wp_nav_menu(array('theme_location' => 'primary', 'container' =>'', 'menu_class' => 'menu')); ?>

// ADD LOCATION MENU
<?php
register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );

add_action( 'after_setup_theme', 'register_primary_menu' );
function register_primary_menu() {
    register_nav_menu( 'primary', __( 'Primary Menu', 'theme-text-domain' ) );
}
?>

//WPML
<?php if(ICL_LANGUAGE_CODE == 'en'){echo 'EN';} else {echo 'JA';} ?>
<?php if(ICL_LANGUAGE_CODE == 'zh-hans'): ?> <?php endif; ?>
<?php if(ICL_LANGUAGE_CODE == 'zh-hans') echo 'CN'; else echo 'JA'; ?>

<?php 
// Chuyển URL qua ngôn ngữ tương ứng với hàm sử dụng
function basetop_url() {
    global $sitepress;
    echo $sitepress->convert_url( home_url(), 'en' );
}
?>



//Trim words the title
<?php echo wp_trim_words( get_the_content(), 30, '...' ); ?>


//Post last in the loop
<?php if($postCount == sizeof($posts)): ?>
<?php endif; ?>
$count = $movies->post_count;


//CONTACT FORM 7
on_sent_ok: "window.location.href='http://design.bpotech.com.vn/nakajima/?page_id=42';"

[radio chooserep class:radio1 default:1 "電話でしてほしい" "FAXでしてほしい" "メールでしてほしい" "返信しなくて良い"]

[hidden store-email default:user_email "example@example.com"]


<?php

//Shortcode demo
// [bartag foo="foo-value"]
function bartag_func( $atts ) {
    $a = shortcode_atts( array(
        'foo' => 'something',
        'bar' => 'something else',
    ), $atts );

    return "foo = {$a['foo']}";
}
add_shortcode( 'bartag', 'bartag_func' );

//Shortcode news-list for-family page
function news_func(){
    $qr = new WP_Query( 'posts_per_page=-1&cat=1' );
    $strNews = "";
    while( $qr->have_posts() ) : $qr->the_post();
    	$strNews .= '<li><span class="date">'.get_the_time('Y.m.d').'</span><span class="etitle"><a href="'.get_permalink().'">'.wp_trim_words(get_the_title(), 50, '...').'</a></span></li>';
    endwhile;
    wp_reset_postdata();
    return $strNews;
}
add_shortcode( 'news', 'news_func' );
//[news]

//Archives Function List WP
wp_get_archives( array( 'post_type' => 'eventreport', 'type' => 'yearly', 'show_post_count' => true, 'after' => '年') );

//Archives Custom Current Yearly
add_filter( "get_archives_link", "customarchives_link");
function customarchives_link( $x ) {
	$url = preg_match('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@i', $x, $matches);
	return $matches[4] == $_SERVER['REQUEST_URI'] ? preg_replace('@<li@', '<li class="current_page_item"', $x) : $x;
}

// Tag Lisst
if(get_the_tag_list()) {
    echo get_the_tag_list('<ul><li>','</li><li>','</li></ul>');
}



//Phan Trang
$pagenum = (get_query_var('paged')) ? get_query_var('paged') : 1;
$postperpage = get_option('posts_per_page');
query_posts('post_type = post & offset='.($pagenum-1)*$postperpage);
?>
<?php if ( $wp_query -> max_num_pages > 0 ) : ?>
    <ul class="ulpager">
        <li class="previous"><?php previous_posts_link('新しい記事へ'); ?></li>
        <li class="next"><?php next_posts_link('古い記事へ'); ?></li>
    </ul>
<?php endif; ?>



//Phan trang trong Single
<ul class="ulpager">
   <li class="previous"><?php previous_post_link( '%link', _x( '新しい記事へ (%title)', 'Previous post link', 'twentythirteen' ) ); ?></li>
   <li class="next"><?php next_post_link( '%link', _x( '古い記事へ', 'Next post link', 'twentythirteen' ) ); ?></li>
</ul>
// Chi Tiết hàm
<?php previous_post_link( $format, $link, $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ); ?>
<li class="prev"><?php previous_post_link( '%link', '<', true, '', 'gallery_type' ); ?></li>

<div class="paging_box paging1">
  <?php previous_post_link( '%link', '<img src="'. get_bloginfo('template_directory'). '/img/news/btn_paging_prev.png" alt="Prev">' ); ?>
  <a class="btncenter" href="/news/">一覧へ</a>
  <?php next_post_link( '%link', '<img src="'. get_bloginfo('template_directory'). '/img/news/btn_paging_next.png" alt="Next">' ); ?>
</div>


//BR For Content WP
<div>&nbsp;</div>
<span>&nbsp;</span>


<?php 
//taxonomy
//- get curent tax
echo get_query_var( 'taxonomy' );
//-get current tax term
echo get_queried_object()->name;

// GET CURENT TAX NAME
single_term_title();

// Get term by name ''news'' in Categories taxonomy.
$category = get_term_by('name', 'news', 'category')
//-> Get object tax filter by name

//Loop Query Tax
$args = array(
    'post_type' => 'post',
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'movie_genre',
            'field'    => 'slug',
            'terms'    => array( 'action', 'comedy' ),
        ),
        array(
            'taxonomy' => 'actor',
            'field'    => 'term_id',
            'terms'    => array( 103, 115, 206 ),
            'operator' => 'NOT IN',
        ),
    ),
);
$query = new WP_Query( $args );


//Loop Query Meta
$args = array(
    'meta_key'     => 'price',
    'meta_value'   => '22',
    'meta_compare' => '<=',
    'post_type'    => 'product'
);
$query = new WP_Query( $args );

$args = array(
    'post_type'  => 'product',
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'color',
            'value'   => 'blue',
            'compare' => 'NOT LIKE',
        ),
        array(
            'key'     => 'price',
            'value'   => array( 20, 100 ),
            'type'    => 'numeric',
            'compare' => 'BETWEEN',
        ),
    ),
);
$query = new WP_Query( $args );
//Note: Check box hay các input có thể trả về nhiều giá trị nên lưu ý dùng mảng
//Note: Truy vấn bị thay đổi với checkbox như sau
 array(
    'key'     => 'prioritize',
    'value'   => 'yes',
    'compare' => 'LIKE',
),


//GET POST META
get_post_meta( int $post_id, string $key = '', bool $single = false );
//Return array is $single false, value is #single true
//Ex: 
$arrpmeta = get_post_meta(get_the_id(), 'english_title');
if($arrpmeta) { $engtitle = $arrpmeta[0]; }


//TIME AGO POST
echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';

//CODE DAY AGO (cách mấy ngày)
$sinceday =  (current_time('timestamp') - get_the_time('U'))/60/60/24;


//COUNT POST TYPE
$count_car = wp_count_posts('post_type');
echo $count_car->publish;


// Get page post Slug
global $post;
$post_slug=$post->post_name;


// Process Child Page WP
function wpb_list_child_pages() { 

    global $post; 
    if ( is_page() && $post->post_parent )
        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
    else
        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
    if ( $childpages ) {
        $string = '<ul>' . $childpages . '</ul>';
    }
    return $string;

}


// Get Page Parent
$parent = get_post_ancestors($post->ID);
if( count( $children ) <= 0  && empty($parent[1]) ) {
    // display full-width layout
} elseif ( count( $children ) <= 0  && !empty($parent[1]) )  {
    // display sidebar-menu layout
}


// Trim 1 Ký tự nào đó trên chuỗi
$listFname = rtrim($listFname,"・");


// Get Post Format
$format = get_post_format( $post_id );
$format = get_post_format();
// Return false or string


// Get All List Custum post type
$args = array(
   'public'   => true,
   '_builtin' => false
);

$output = 'names'; // names or objects, note names is the default
$operator = 'and'; // 'and' or 'or'

$post_types = get_post_types( $args, $output, $operator ); 

foreach ( $post_types  as $post_type ) {

   echo '<p>' . $post_type . '</p>';
}



//*** BEGIN SETUP WIDGET ***//
/**
 * Adds A Example widget.
 */
class RC_Posttype_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'rcpt_widget', // Base ID
            esc_html__( 'Recent Post Type Widget', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'A Recent Post Type Widget', 'text_domain' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        ?>

        <ul>
            <?php 
                $args = array(
                    'post_type' => $instance['posttype'], 
                    'posts_per_page' => $instance['numpost'],
                    'category_name' => $instance['category'],
                );
                $news = new WP_Query($args);
                while($news->have_posts()) : $news->the_post();
            ?>
                <li>
                    <p><a href="<?php the_permalink(); ?>" class="hoverJS"><?php the_title(); ?></a></p>
                </li>   
            <?php endwhile; wp_reset_postdata(); ?>
            
        </ul>
        <?php
        echo $args['after_widget'];

    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php 

        $numpost = ! empty( $instance['numpost'] ) ? $instance['numpost'] : esc_html__( '0', 'number' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'numpost' ) ); ?>"><?php esc_attr_e( 'Numpost:', '0' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'numpost' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'numpost' ) ); ?>" type="text" value="<?php echo esc_attr( $numpost ); ?>">
        </p>
        <?php 

        $posttype = !empty( $instance['posttype']) ? $instance['posttype'] : esc_html__( 'Post Type', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('posttype') ); ?>"><?php esc_attr_e('Post Type:','') ?></label>
            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('posttype')); ?>" name="<?php echo esc_attr($this->get_field_name('posttype')); ?>" type="text">
            <?php
                $args = array(
                    'public'   => true,
                    '_builtin' => false,
                );
                $output = 'names'; 
                $operator = 'and';
                $post_types = get_post_types( $args, $output, $operator );
                echo '<option value="post" id="post"', $posttype == 'post' ? ' selected="selected"' : '', '>', ucfirst('post'), '</option>';
                foreach ($post_types as $post_type) {
                    echo '<option value="' . $post_type . '" id="' . $post_type . '"', $posttype == $post_type ? ' selected="selected"' : '', '>', ucfirst($post_type), '</option>';
                }
            ?>
            </select>
        </p>
        <?php

        $category = !empty( $instance['category']) ? $instance['category'] : esc_html__( 'Category', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('category') ); ?>"><?php esc_attr_e('Category:','') ?></label>
            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('category')); ?>" name="<?php echo esc_attr($this->get_field_name('category')); ?>" type="text">
            <?php
                $cats = get_categories();
                foreach ($cats as $cat) {
                    echo '<option value="' . $cat->name . '" id="' . $cat->name . '"', $category == $cat->name ? ' selected="selected"' : '', '>', $cat->name, '</option>';
                }
            ?>
            </select>
        </p>

    <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['numpost'] = ( ! empty( $new_instance['numpost'] ) ) ? strip_tags( $new_instance['numpost'] ) : '';
        $instance['posttype'] = ( ! empty( $new_instance['posttype'] ) ) ? strip_tags( $new_instance['posttype'] ) : '';
        $instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';

        return $instance;
    }

} // class Foo_Widget


// register Foo_Widget widget
function register_rcpt_widget() {
    register_widget( 'RC_Posttype_Widget' );
}
add_action( 'widgets_init', 'register_rcpt_widget' );

//*** END SETUP WIDGET ***//


//công thêm 10 ngày hoặ trừ đi 10 ngày.
$now = date('2014-11-18 14:10');
$date = new DateTime($now);
$days = 10;
// trừ đi 10 ngày
// >= php version 5.3
date_sub($date, date_interval_create_from_date_string($days.' days'));
$date_minus = date_format($date, 'Y-m-d H:i');
//output 2014-11-08 14:10


//Công trừ thời gian ra số ngày trong PHP
$date = date("2015-08-12");
$day_1 = date('Y-m-d', $date) ;
$day_2 = date('Y-m-d') ; //current date
            
echo $days = (strtotime($day_1) - strtotime($day_2)) / (60 * 60 * 24);
//ouput so ngay


add_image_size( 'news-thumb', 250, 135, true );


// CUSTOM RSS
function custom_rss_feed() {
    global $post;
    $output = '';
    $thumbnail_ID = get_post_thumbnail_id( $post->ID );
    $thumbnail = wp_get_attachment_image_src($thumbnail_ID, 'news-thumb');
    $output .= '<post-thumbnail>';
    $output .= '<url>'. $thumbnail[0] .'</url>';
    $output .= '<width>'. $thumbnail[1] .'</width>';
    $output .= '<height>'. $thumbnail[2] .'</height>';
    $output .= '</post-thumbnail>';
    echo $output;
}
add_action('rss2_item', 'custom_rss_feed');




?>


// JS Click Mixup
<script type="text/javascript">
    $(document).ready(function(){
        var crrurl = window.location.href;
        crrurl = crrurl.split('/');
        var tabname = crrurl.pop();
        tabname = tabname.replace('#', '');

        $('.area1 .controls li').each(function(){
            var dtbtn = $(this).find('button').attr('data-filter').replace('.', '');
            if( dtbtn == tabname ) {
                $(this).find('button').click();
            }
        });
    });
</script>


// JS Contact Form 7 Custom Confirm Step
<script type="text/javascript">
    function addClassStep(step_name) {
        var divcf7 = jQuery('.wpcf7');
        divcf7.removeClass('Step1').removeClass('Step2');
        divcf7.addClass(step_name);
        console.log('****_ add class |Cf7classStep| for auto add class step form _****');
        $('.Cf7classStep').removeClass('Step1').removeClass('Step2').addClass(step_name);

        // Del select box nit checked
        if( step_name == 'Step2' ) {
            $('.wpcf7-form .wpcf7-checkbox .wpcf7-list-item').hide();
            $('.wpcf7-form .wpcf7-checkbox .wpcf7-list-item').each(function(){
                if( $(this).find('input').is(':checked') ) {
                    $(this).show();
                }
            });
        }
        else {
            $('.wpcf7-form .wpcf7-checkbox .wpcf7-list-item').show();
        }
    }
</script>


// CF7 SENDMAIL OK EVENT
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '153' == event.detail.contactFormId ) {
        location.href = '/contact-finish/';
    }
}, false );
</script>


// GET DATA XML
<script type="text/javascript">
    $.ajax({
        url: '/html/template/default/img/common/svg.html',
        type: 'GET',
        dataType: 'html',
    }).done(function(data) {
        $('body').prepend(data);
    }).fail(function(data) {});

    $.get('/', function(data) {
        // $('#header').html($(data).find('#header').html().replace('?','¥'));
        $('#header').html($(data).find('#header').html());
        $('#drawer').html($(data).find('.drawer_block').clone(true).children());
        $('#footer').html($(data).find('#footer').html());
    });
</script>