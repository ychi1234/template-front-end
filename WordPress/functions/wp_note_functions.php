<?php
	


// Add RSS links to <head> section
automatic_feed_links();

// Load jQuery
if ( !is_admin() ) {
   wp_deregister_script('jquery');
   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"), false);
   wp_enqueue_script('jquery');
}

// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// Declare sidebar widget zone
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Left Sidebar',
		'id'   => 'left-sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

// Declare sidebar widget zone
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Right Sidebar',
		'id'   => 'right-sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

//Build Menu Navigation
if (function_exists('register_nav_menus')) {
    register_nav_menus(array(
        'main_nav' => 'Main Navigation Menu'
    ));
}

//Post Thumbnail
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

//Excerpt show Link
function new_excerpt_more($more){
    global $post;
    return "<a class='more-link' href='".get_permalink($post->ID)."'>Read more</a>";
}
add_filter('excerpt_more','new_excerpt_more');

//Excerpt length content
function new_excerpt_length($more){
    return 40;
}
add_filter('excerpt_length','new_excerpt_length');


//Theme Option Origin
include_once(TEMPLATEPATH. '/inc/theme-options.php');

//SO Sanh Tag ở Post Hiện Tại
function compcurrtag($tagname) {
global $post;

$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    if($tag->name == $tagname) {
        return true;
    } 
  }
}
return false;
}


// so sanh list id cat hien tai voi 1 id cat khac 
function compArrayCat($cat_id) {
    global $post;

    $categories = get_the_category($post->ID);
    foreach($categories as $category) {
        if($category->term_id == $cat_id) {
            return true;
        }
    }

    return false;
}


//Show list category in curent post
function showCatsInPost() {
    global $post;

    $strcats = '';
    $categories = get_the_category($post->ID);
    foreach($categories as $category) {
        if($category->term_id != 10) {
            $strcats .= '<li><strong><a href="'. get_category_link($category->term_id) .'">'.$category->name.'</a></strong>,</li>'; 
        }
    }
    return $strcats;
}

//Show list tag in current post
function showTagsInPost() {
    global $post;

    $strtags = '';
    $posttags = get_the_tags();
    if ($posttags) {
      foreach($posttags as $tag) {
        $strtags .= '<li><a href="'. get_tag_link($tag->term_id) .'" rel="tag">'. $tag->name .'</a><span class="kigou">,</span></li>';
      }
    }
    return $strtags;
}

/*** Show Taxonomy in Post ***/
function showTaxsInPost($taxslug = 'category') {
    global $post;
    if( !$taxslug ) {
        $taxslug = 'category';
    }
    $strtaxs = '';
    $taxonomies = wp_get_post_terms( $post->ID, $taxslug);
    foreach($taxonomies as $taxonomy) {
        $classtax = 'cat1';
        if($taxonomy->description) {
            $classtax = $taxonomy->description;
        }
        $strtaxs .= '<span class="' .$classtax. '">' .$taxonomy->name. '</span>';
    }
    return $strtaxs;
}

/*** Show List Taxonomy Tab ***/
function showListTaxTab($taxslug) {
    if( !$taxslug ) {
        $taxslug = 'category';
    }
    $strtaxs = '';
    $taxonomies = get_terms( $taxslug );
    foreach($taxonomies as $taxonomy) {
        $strtaxs .= '<button class="filter" data-filter=".' .$taxonomy->slug. '">' .$taxonomy->name. '</button>';
    }
    return $strtaxs;
}

/*** Get List Taxonomy ***/
function getListTax($taxslug = 'category') {
    if( !$taxslug ) {
        $taxslug = 'category';
    }
    $strtaxs = '';
    $taxonomies = get_terms( $taxslug );
    foreach($taxonomies as $taxonomy) {
        $strtaxs .= '<li><a href="'. get_term_link($taxonomy) .'">'.$taxonomy->name.'</a></li>';
    }
    return $strtaxs;
}

/*** Show List Category ***/
function showListCat() {
    $categories = get_categories();
    $strcats = '';
    foreach($categories as $category) {
        $strcats .= '<li><a href="'. get_category_link($category->term_id) .'">'.$category->name.'</a></li>';
    }
    return $strcats;
}

/*** Show List Tax Post Type ***/
function listtaxPosttype($taxslug, $posttype='post') {
    if( !$taxslug ) {
        $taxslug = 'category';
    }
    $strtaxs = '';
    $taxonomies = get_terms( $taxslug );
    foreach($taxonomies as $taxonomy) {
        $taxlink = get_term_link($taxonomy);
        $keyval = '?';
        if( parse_url($taxlink, PHP_URL_QUERY) ) {
            $keyval = '&';
        }
        $strtaxs .= '<li><a href="' .$taxlink.$keyval.'post_type='.$posttype. '">'.$taxonomy->name.'</a></li>';
    }
    return $strtaxs;
}


// CUT CONTENT FUNCTION
function cut_content_text( $para = '', $numchar = 100 ) {
    if( mb_strlen( $para, 'UTF-8' ) > $numchar ) {
        $content = mb_substr(strip_tags($para), 0, $numchar, 'UTF-8');
        return $content.'…';
    }else{
        return strip_tags($para);
    }
}


//PAGING ALL FUNCTION

// *** Paging Template List *** //
function mfn_pagination()
{
    global $paged, $wp_query;
    
    $translate['next'] = '>>';
    $translate['prev'] = '<<';
    
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;  

    if( ! $total = $wp_query->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="pager">';
        echo '<center>';
        
        if( $paged >1 ){
            echo '<a class="next page-numbers" href="'. previous_posts(false) .'">'. $translate['prev'] .'</a>';
        }

        for( $i=1; $i <= $total; $i++ ){
            if ( $i == $current ){
                echo '<span class="page-numbers current">'. $i .'</span>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<a class="page-numbers" href="'. get_pagenum_link($i) .'">'. $i .'</a>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<span class="page-numbers dots">...</span>';
                    $dots = false;
                }
            }
      }
      
      if( $paged < $total ){
          echo '<a class="next page-numbers" href="'. next_posts(0,false) .'">'. $translate['next'] .'</a>';
      }

      echo '</center>';
      echo '</div>'."\n";
    }
}


//***********************************************//
///////// Hàm Phân Trang Cải Tiến ////////////////
// *** Paging Template Function *** //
function mfntemp_pagination( $postquery = '' ) {
    global $paged, $wp_query;
    
    $translate['next'] = '>';
    $translate['prev'] = '<';
    
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;

    $mainquery = $wp_query;
    if( isset($postquery) && $postquery ) {
        $mainquery = $postquery;
    }

    if( ! $total = $mainquery->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="pagingNav">';
        echo '<ul class="pagi_nav_list">';
        
        if( $paged >1 ){
            echo '<li class="p-control prev"><a href="'. previous_posts(false) .'">'. $translate['prev'] .'</a></li>';
        }

        for( $i=1; $i <= $total; $i++ ){
            if ( $i == $current ){
                echo '<li class="current"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li><a href="'. get_pagenum_link($i) .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }
      
        if( $paged < $total ){
            echo '<li class="p-control next"><a href="'. next_posts(0,false) .'">'. $translate['next'] .'</a></li>';
        }

      echo '</ul>';
      echo '</div>'."\n";
    }
}



// *** Paging WP_Query List *** //
// -- Function -- //
function mfn_pagination_static($postquery)
{
    global $paged, $wp_query;
    
    $translate['next'] = '>';
    $translate['prev'] = '<';
    
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;                         
    $next = $paged + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;  

    if( ! $total = $postquery->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="nav-page">';
        echo '<ul class="pager">';
        
        if( $paged >1 ){
            echo '<li class="pcontrol prev"><a href="'. previous_posts(false) .'">'. $translate['prev'] .'</a></li>';
        }
        else {
            echo '<li class="pcontrol prev">'. $translate['prev'] .'</li>';
        }

        for( $i=1; $i <= $total; $i++ ){
            if ( $i == $current ){
                echo '<li class="page active">'. $i .'</li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li class="page"><a href="'. get_pagenum_link($i) .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<span class="page">...</span>&nbsp;';
                    $dots = false;
                }
            }
        }
        
        if( $paged < $total ){
            echo '<li class="pcontrol next"><a href="'. next_posts(0,false) .'">'. $translate['next'] .'</a></li>';
        }
        else {
            echo '<li class="pcontrol next">'. $translate['next'] .'</li>';
        }

        echo '</ul>';
        echo '</div>'."\n";
    }
}

// -- Use Setting -- //
$pagenum = (get_query_var('paged')) ? get_query_var('paged') : 1;
$postperpage = get_option('posts_per_page');
query_posts('post_type = post & offset='.($pagenum-1)*$postperpage);

// -- Get Offset Paging -- //
function getopffset(){
    $pagenum = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $postperpage = get_option('posts_per_page');
    return ($pagenum-1)*$postperpage;
}

// Get Offset paging
function getopffset( $postpage = 0 ){
    $pagenum = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $postperpage = get_option('posts_per_page');
    if( $postpage != 0 ) {
        $postperpage = $postpage;
    }
    return ($pagenum-1)*$postperpage;
}



// *** Paging WP_Query List GET PAGING *** //
// -- Function -- //
function mfn_pagination_getpaging($postquery)
{
    global $paged, $wp_query;
    
    $translate['next'] = '<img src="'.get_bloginfo('template_directory').'/img/faq/next.png">';
    $translate['prev'] = '<img src="'.get_bloginfo('template_directory').'/img/faq/prev.png">';
    
    isset($_GET['paging'])&&$_GET['paging'] > 1 ? $current = $_GET['paging'] : $current = 1;  
    
    $prev = $current - 1;
    $next = $current + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;  

    if( ! $total = $postquery->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<ul class="control">';

        if( $current >1 ){
            echo '<li class="prev hoverJS"><a href="?paging='. $prev .'">'. $translate['prev'] .'</a></li>';
        }
        else {
            echo '<li class="prev hoverJS"><a>'. $translate['prev'] .'</a></li>';
        }

        for( $i=1; $i <= $total; $i++ ){
            if ( $i == $current ){
                echo '<li class="active"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li><a href="?paging='. $i .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<li class="active"><a>...</a></li>';
                    $dots = false;
                }
            }
        }

        if( $current < $total ){
            echo '<li class="next hoverJS"><a href="?paging='. $next .'">'. $translate['next'] .'</a></li>';
        }
        else {
            echo '<li class="next hoverJS"><a>'. $translate['next'] .'</a></li>';
        }

        echo '</ul>'."\n";
    }
}

// -- Use Setting -- //
isset($_GET['paging'])&&$_GET['paging'] > 1 ? $offpage = $_GET['paging'] : $offpage = 1;
$postperpage = get_option('posts_per_page');
$postvoice = new WP_Query(array(
    'offset' => ($offpage-1)*$postperpage,
    'posts_per_page' => $postperpage, 
    'post_type' => 'postvoice'
));


// Get Offset paging
function getopffset_method( $postpage = 0 ){
    isset($_GET['paging'])&&$_GET['paging'] > 1 ? $pagenum = $_GET['paging'] : $pagenum = 1;
    $postperpage = get_option('posts_per_page');
    if( $postpage != 0 ) {
        $postperpage = $postpage;
    }
    return ($pagenum-1)*$postperpage;
}


// Paging Fist Last
function mfn_pagination()
{
    global $paged, $wp_query;
    
    $translate['next'] = '<img src="'.THEME_URL.'/img/list/icon_next.png" alt="Next">';
    $translate['prev'] = '<img src="'.THEME_URL.'/img/list/icon_prev.png" alt="Next">';
    $translate['last'] = '<img src="'.THEME_URL.'/img/list/icon_next1.png" alt="Next">';
    $translate['first'] = '<img src="'.THEME_URL.'/img/list/icon_prev1.png" alt="Next">';
    
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;  

    if( ! $total = $wp_query->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="slider-page">';
        
        if( $paged > 1 ){
            echo '<p class="prev first"><a class="hoverJS" href="'. get_pagenum_link(1) .'">'. $translate['first'] .'</a></p>';
            echo '<p class="prev"><a class="hoverJS" href="'. previous_posts(false) .'">'. $translate['prev'] .'</a></p>';
        }
        else {
            echo '<p class="prev first hiden"><a class="hoverJS" href="'. get_pagenum_link(1) .'">'. $translate['first'] .'</a></p>';
            echo '<p class="prev hiden"><a class="hoverJS" href="'. previous_posts(false) .'">'. $translate['prev'] .'</a></p>';
        }

        echo '<ul class="pager">';

        for( $i=1; $i <= $total; $i++ ){
            if ( $i == $current ){
                echo '<li class="active"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li><a href="'. get_pagenum_link($i) .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }

        echo '</ul>';
        
        if( $paged < $total ){
            echo '<p class="next last"><a class="hoverJS" href="'. get_pagenum_link($total) .'">'. $translate['last'] .'</a></p>';
            echo '<p class="next"><a class="hoverJS" href="'. next_posts(0,false) .'">'. $translate['next'] .'</a></p>';
        }
        else {
            echo '<p class="next last hiden"><a class="hoverJS" href="'. get_pagenum_link($total) .'">'. $translate['last'] .'</a></p>';
            echo '<p class="next hiden"><a class="hoverJS" href="'. next_posts(0,false) .'">'. $translate['next'] .'</a></p>';
        }

        echo '</div>';
    }
}


// Get Number on 4 charater
function getnum4char($number) {
    $strnum = strval($number);
    $strlenzr = 4 - strlen($strnum);
    if( $strlenzr >= 0 ) {
        return str_repeat("0", $strlenzr) . $strnum;
    }
    return $strnum;
}



// ACF CUSTOM FUNCTION

// THE MAIN IMAGE
function the_field_image( $field_name, $size ) {
    global $post;
    if( get_field($field_name) ) {
        $imgobj = get_field($field_name);
        if( $size == 'full' ) {
            echo '<img class="main_image" src="'.$imgobj['url'].'">';
        }
        else {
            echo '<img class="main_image" src="'.$imgobj['sizes'][ $size ].'">';
        }
    }
}

// Function get list choose fields ACF
function getlistchooseAcf($posttype, $fieldname) {
    $fpost = 0;
    $reschoose = '';
    //Get first post type
    $wppost = new WP_Query(array('posts_per_page'=>'1', 'post_type'=>$posttype));
    while($wppost->have_posts()) : $wppost->the_post();
        $fpost = get_the_ID();
    endwhile; wp_reset_postdata();
    if( $fpost ) {
        $ojfields = get_field_objects($fpost);
        $ojfield = $ojfields[$fieldname];
        $ojchoose = $ojfield['choices'];
        if( $ojchoose ) {
            $reschoose .= '<select name="choose">';
            foreach ($ojchoose as $key => $value) {
                $reschoose .= '<option value="' .$key. '">' .$value. '</option>';
            }
            $reschoose .= '</select>';
        }
    }
    return $reschoose;
}

// Custom select field create opt group
add_filter('acf/load_field/name=post_select', 'acf_create_select');
function acf_create_select( $field ) {
    $screen = get_current_screen();
    if( $screen && $screen->id != 'acf' ) {
        $fkeyw = '#';
        $choices = $field['choices'];
        $reschoices = array();
        $flag = '';
        foreach ($choices as $key => $value) {
            if( strrpos($value, $fkeyw) === 0 ) {
                $flag = str_replace($fkeyw, '', $value);
                $reschoices[$flag] = array();
            }
            else if( $flag ) {
                $reschoices[$flag][$key] = $value;
            }
            else {
                $reschoices[$key] = $value;
            }
        }
        $field['choices'] = $reschoices;
    }
    return $field;
}


// END ACF




// POST DATA //

// CUSTOM DATA BEFORE SAVE POST
function filter_handler( $data , $postarr ) {
    // do something with the post data
    if( $postarr['post_type'] == 'shoplist' ) {
        $str_add_content = '';
        $my_post_id = $postarr['ID'];
        $arr_sfield = array('store_businesstime', 'store_holiday', 'store_parkinglot', 'store_map');
        foreach ($arr_sfield as $sfield) {
            if( get_field( $sfield, $my_post_id ) ) {
                //$str_add_content .= get_field( $sfield, $my_post_id ) . '<br>';
                $field = get_field_object($sfield, $my_post_id);
                $field_key = $field['key'];
                if( $sfield == 'store_map' ) {
                    $get_post_field = $_POST['acf'][$field_key]['address'];
                } else {
                    $get_post_field = $_POST['acf'][$field_key];
                }
                $str_add_content .= $get_post_field . '<br>';
            }
        }
        $data['post_content'] = $str_add_content;
    }
    return $data;
}

add_filter( 'wp_insert_post_data', 'filter_handler', '99', 2 );




/*===========================================*/
/*  Popular Post View
/*===========================================*/

function wpb_set_post_views($postID) {

    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;   
    }
    wpb_set_post_views($post_id);
}

add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){

    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

/*===========================================*/
/*  Post Thumbnail Popular Top Page
/*===========================================*/

$popularpost = new WP_Query( array( 'posts_per_page' => 4, 
    'meta_key' => 'wpb_post_views_count', 
    'orderby' => 'meta_value_num', 
    'order' => 'DESC'  ) );





// Get Nav Link Single Post Type
function navlinkpost($nav, $taxslug, $posttype) {
    global $post;
    if( !$taxslug ) {
        $taxslug = 'category';
    }
    $arrpost = array();
    //Get this taxs
    $currtax = array();
    $taxonomies = wp_get_post_terms( $post->ID, $taxslug);
    foreach($taxonomies as $taxonomy) {
        if( $taxonomy->slug != 'all' )
        array_push($currtax, $taxonomy->term_id);
    }
    //Get list Post
    $listPost = new WP_Query(array(
        'posts_per_page' => '-1', 
        'post_type' => $posttype,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => $taxslug,
                'field'    => 'term_id',
                'terms'    => $currtax,
            ),
        ),
    ));
    while($listPost->have_posts()) : $listPost->the_post();
        array_push($arrpost, get_the_ID());
    endwhile; wp_reset_postdata();
    //Process Nav
    $postlength = count($arrpost);
    $keyid = array_search($post->ID, $arrpost);
    if( $nav == 'next' && $keyid+1 < $postlength ) {
        $nextpid = $arrpost[$keyid+1];
        echo '<a href="'.get_permalink($nextpid).'"> > </a>';
    }
    elseif( $nav == 'prev' && $keyid-1 >= 0 ) {
        $prevpid = $arrpost[$keyid-1];
        echo '<a href="'.get_permalink($prevpid).'"> < </a>';
    }
    // var_dump($arrpost);
    // var_dump($keyid);
}


// https://contactform7.com/2017/06/07/on-sent-ok-is-deprecated/
// CF7 Sendmail Ok Process
add_action( 'wp_footer', 'jssendmail_wp_footer' );
function jssendmail_wp_footer() {
?>
    <script type="text/javascript">
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        if ( '153' == event.detail.contactFormId ) {
            location.href = '/contact-finish/';
        }
    }, false );
    </script>
<?php
}

// BREADCRUMB MULTILANG
add_filter('bcn_breadcrumb_title', function($title, $type, $id) {
    if ($type[0] === 'home') {
        $title = 'TOP';
    }
    return $title;
}, 42, 3);

add_filter('bcn_breadcrumb_url', function($url, $type, $id) {
    if ($type[0] === 'home') {
        // var_dump($url);
        $url = HOME_URL;
    }
    return $url;
}, 42, 3);

function cs_custom_posts_per_page( $query ) {
    if ( is_tax( 'construction_cat' ) ) {
        $query->query_vars['posts_per_page'] = -1;
        return;
    }
}
add_filter( 'pre_get_posts', 'cs_custom_posts_per_page' );
?>