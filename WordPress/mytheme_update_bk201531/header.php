<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/common.css">
    <?php if( is_home() || is_front_page() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/index.css">
    <?php elseif( is_post_type_archive('voice') ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/voice.css">
    <?php elseif( is_category() || is_archive() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/blog.css">
    <?php elseif( is_single() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/blog-detail.css">
    <?php
        elseif( is_page() ):
        global $post;
        $post_slug = $post->post_name;
        $page_css_uri = get_template_directory().'/css/'.$post_slug.'.css';
        if( is_file($page_css_uri) ):
    ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/css/<?php echo $post_slug; ?>.css">
    <?php endif; endif; ?>
    
    <script type="text/javascript" src="<?php echo THEME_URL; ?>/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo THEME_URL; ?>/js/script.js"></script>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			</a>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php _e( 'Menu', 'mytheme' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'mytheme' ); ?>"><?php _e( 'Skip to content', 'mytheme' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><!-- #masthead -->

		<div id="main" class="site-main">
