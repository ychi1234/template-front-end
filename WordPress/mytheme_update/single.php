<?php
/**
 * The template for displaying all single posts
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<?php if ( is_single() ) : ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<?php else : ?>
						<h1 class="entry-title">
							<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h1>
						<?php endif; // is_single() ?>

						<div class="entry-meta">
							<?php mytheme_entry_meta(); ?>
							<?php edit_post_link( __( 'Edit', 'mytheme' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->

					<?php if ( is_search() ) : // Only display Excerpts for Search ?>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content">
						<?php
							/* translators: %s: Name of current post */
							the_content( sprintf(
								__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'mytheme' ),
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );

							wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mytheme' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
						?>
					</div><!-- .entry-content -->
					<?php endif; ?>

					<footer class="entry-meta">
						<?php if ( comments_open() && ! is_single() ) : ?>
							<div class="comments-link">
								<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'mytheme' ) . '</span>', __( 'One comment so far', 'mytheme' ), __( 'View all % comments', 'mytheme' ) ); ?>
							</div><!-- .comments-link -->
						<?php endif; // comments_open() ?>

						<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
							<div class="author-info">
								<div class="author-avatar">
									<?php
									/**
									 * Filter the author bio avatar size.
									 *
									 * @since My Theme 1.0
									 *
									 * @param int $size The avatar height and width size in pixels.
									 */
									$author_bio_avatar_size = apply_filters( 'mytheme_author_bio_avatar_size', 74 );
									echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
									?>
								</div><!-- .author-avatar -->
								<div class="author-description">
									<h2 class="author-title"><?php printf( __( 'About %s', 'mytheme' ), get_the_author() ); ?></h2>
									<p class="author-bio">
										<?php the_author_meta( 'description' ); ?>
										<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
											<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'mytheme' ), get_the_author() ); ?>
										</a>
									</p>
								</div><!-- .author-description -->
							</div><!-- .author-info -->
						<?php endif; ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>