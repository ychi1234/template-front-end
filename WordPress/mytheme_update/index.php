<?php
/**
 * The main template file
 */

get_header(); ?>

	<div id="primary">
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php endif; ?>
	</div>
	<!-- #content -->

<?php get_footer(); ?>