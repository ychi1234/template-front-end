<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/common.css">
    <?php if( is_home() || is_front_page() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/index.css">
    <?php elseif( is_post_type_archive('voice') ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/voice.css">
    <?php elseif( is_category() || is_archive() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/blog.css">
    <?php elseif( is_single() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/blog-detail.css">
    <?php
        elseif( is_page() ):
        global $post;
        $post_slug = $post->post_name;
        $page_css_uri = THEME_PATH.'/assets/css/'.$post_slug.'.css';
        if( is_file($page_css_uri) ):
    ?>
        <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/assets/css/<?php echo $post_slug; ?>.css">
    <?php endif; endif; ?>
    
    <script type="text/javascript" src="<?php echo THEME_URL; ?>/assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo THEME_URL; ?>/assets/js/script.js"></script>
</head>

<body <?php body_class(); ?>>
    <div id="header">
        <?php echo HOME_URL; ?>
        <?php bloginfo( 'name' ); ?>
        <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
        <?php bloginfo( 'description' ); ?>
        <?php wp_nav_menu(array('theme_location' => 'primary', 'container' =>'', 'menu_class' => 'menu')); ?>
        <?php wp_nav_menu(array('menu' => 'mainMenu', 'container' => '', 'menu_class' => 'menu')); ?>
        <?php get_search_form(); ?>
        <?php _e( 'Text', 'mytheme' ); ?>
    </div>