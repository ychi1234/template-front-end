
<?php 
    global $page_name, $title_page;

    // ACTIVE MENU
    // $active_pages = array(
    //     'index' => '', 
    //     'hakajimai' => '', 
    //     'service' => '', 
    //     'flow' => '', 
    //     'contact' => '', 
    // );

    // if (isset($page_name)  &&  isset($active_pages[$page_name]) ) {
    //     $active_pages[$page_name] = 'active';
    // }
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,500,700&display=swap" rel="stylesheet">
    <title><?php echo $title_page; ?> | TEMPLATE</title>
    
    <meta name="description" content=" content " />
    <meta name="keywords" content=" content " />
    <meta name="author" content=" content " />
    <meta name="robots" content=" all " />
    <meta name="googlebot" content=" all ">
    <link rel="stylesheet" type="text/css" href="assets/css/common.css">
    <?php if (isset($page_name) ) : ?>
        <link rel="stylesheet" type="text/css" href="assets/css/<?php echo $page_name; ?>.css">
    <?php endif; ?>
    <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</head>
<body>

<div id="header">
</div>
<!-- #header -->

<?php //if( isset($page_name) && $page_name == 'index' ): ?>

<?php //else: ?>