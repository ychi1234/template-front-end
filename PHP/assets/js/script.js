/***************************************************************************
*
* SCRIPT JS
*
***************************************************************************/

$(document).ready(function(){

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });


    //SCROLL TO TOP 
    if ($(window).width() <= 768) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 80) {
                $('.scrollToTop').fadeIn(400);
            } else {
                $('.scrollToTop').fadeOut(400);
            }
        });
    }

});