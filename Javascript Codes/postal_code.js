/**
 * POSTAL CODE JS  
 */


$(document).ready(function(){

	var $postalCodeDom = $('#contact_zipcode');
	var $addressDom = $('#contact_address');
	var apiKey = 'AIzaSyBCX8kPzEC8cn-12QGals7k2nVa1JS_gx8';

	$postalCodeDom.on('keyup', function(){
		var postalcode = $(this).val();
		var apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?&address=' + postalcode + '&language=ja&key=' + apiKey;
		$('#alertPostalCode').remove();

		// PROCESS POSTAL CODE
		if( postalCodeValidate(postalcode) ) {
			$.getJSON(apiUrl, function(response) {
				if( response ) {
					address_data = response.results[0].address_components;
					if( address_data.length >= 5 ) {
						state = address_data[1].long_name;
						city = address_data[2].long_name;
						placename = address_data[3].long_name;
						country = address_data[4].long_name;
						address = placename + city + state;
						console.log(address);
						$addressDom.val( address );
					} else {
						$postalCodeDom.after( getSpanError('郵便番号が検索されません') );
					}
				} else {
					$postalCodeDom.after( getSpanError('郵便番号が検索されません') );
				}
			});
		} else {
			$postalCodeDom.after( getSpanError('郵便番号は正しくない') );
		}
	});

}); 


// GET SPAN ERROR
function getSpanError( message ) {
	if( message ) {
		return '<span role="alert" id="alertPostalCode" class="wpcf7-not-valid-tip">' + message + '</span>';
	}
	return '';
}


// VALIDATE POSTAL CODE
function postalCodeValidate( postcode ) {
	var regPostcode1 = /^([0-9]){3}[-]([0-9]){4}$/;
	var regPostcode2 = /^([0-9]){7}$/
	if(regPostcode1.test(postcode) == true || regPostcode2.test(postcode) == true){
		return true;
	}
	return false;
}