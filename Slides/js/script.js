/***************************************************************************
*
* SCRIPT JS
*
***************************************************************************/

$(document).ready(function(){

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });


    //SCROLL TO TOP 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 80) {
            $('.scrollToTop').fadeIn(400);
        } else {
            $('.scrollToTop').fadeOut(400);
        }
    });


    //SCROLL ANCHOR
    $('.anchor a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if($(window).width() > 768) {
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 50
                    }, 1000);
                    return false;
                }
            } else {
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 25
                    }, 1000);
                    return false;
                }
            }
        }
    });


    $('#nav-icon1').click(function(){
        $(this).toggleClass('open');
        $('#header .hdContent .mainMenu').slideToggle('slow');
    });


    if( $(window).width() < 769 ){
        height_footer_fixed();
        $(window).resize(function(){
            height_footer_fixed();
        });
    }
    function height_footer_fixed(){
        var h_entry = $('.foot_entry').outerHeight();
        $('.height_foot').css('height', h_entry);

    }


});