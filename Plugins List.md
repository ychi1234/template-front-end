## update by name
##### date
Name Plugin: http://git.bpotech.com.vn
```bash
Description
```

## update by NamTNH
##### 15/01/2020
###### Name Plugin: All-in-One WP Migration
###### Link Download: https://wordpress.org/plugins/all-in-one-wp-migration/
```bash
This plugin exports your WordPress website including the database, media files, plugins and themes with no technical knowledge required.
Upload your site to a different location with a drag and drop in to WordPress.
```

## update by NamTNH
##### 22/08/2019
###### Name Plugin: ACF
###### Link Download: https://wordpress.org/plugins/advanced-custom-fields/
```bash
Use the Advanced Custom Fields plugin to take full control of your WordPress edit screens & custom field data.
```

###### Name Plugin: ACF REPEATER
```bash
The repeater field allows you to create a set of sub fields which can be repeated again and again whilst editing content!
Any type of field can be added as a sub field which allows you to create and manage very customized data with ease!
```

###### Name Plugin: ACF OPTION PAGE
```bash
The options page feature provides a set of functions to add extra admin pages to edit ACF fields! Each admin page can be fully customized (see code examples below), 
and sub admin pages can be created too!
```

###### Name Plugin: ACF FLEXIBLE CONTENT
```bash
The flexible content field acts as a blank canvas to which you can add an unlimited number of layouts with full control over the order.
Each layout can contain 1 or more sub fields allowing you to create simple or complex flexible content layouts.
```

###### Name Plugin: ACF TABLE FIELD
###### Link Download: https://wordpress.org/plugins/advanced-custom-fields-table-field/
```bash
The Table Field Plugin enhances the functionality of the “Advanced Custom Fields” plugin with easy-to-edit tables.
```

###### Name Plugin: WordPress Infinite Scroll – Ajax Load More
###### Link Download: https://wordpress.org/plugins/ajax-load-more/
```bash
Ajax Load More is the ultimate WordPress infinite scroll plugin for lazy loading posts, single posts, pages, comments and more with Ajax powered queries.
```

###### Name Plugin: All in One SEO Pack
###### Link Download: https://wordpress.org/plugins/all-in-one-seo-pack/
```bash
Use All in One SEO Pack to optimize your WordPress site for SEO. It’s easy and works out of the box for beginners, 
and has advanced features and an API for developers.
```

###### Name Plugin: Admin Menu Editor
###### Link Download: https://wordpress.org/plugins/admin-menu-editor/
```bash
Admin Menu Editor lets you manually edit the Dashboard menu. You can reorder the menus, show/hide specific items, change premissions, and more.
```

###### Name Plugin: Admin Columns
###### Link Download: https://wordpress.org/plugins/codepress-admin-columns/
```bash
Manage and organize columns in the posts, users, comments and media lists in the WordPress admin panel.
Transform the WordPress admin screens into beautiful, clear overviews.
```

###### Name Plugin: Yoast SEO
###### Link Download: https://wordpress.org/plugins/wordpress-seo/
```bash
Need some help with your search engine optimization? Need an SEO plugin that helps you reach for the stars?
Yoast SEO is the original WordPress SEO plugin since 2008. 
It is the favorite tool of millions of users, ranging from the bakery around the corner to some of the most popular sites on the planet. 
With Yoast SEO, you get a solid toolset that helps you aim for that number one spot in the search results. Yoast: SEO for everyone.
```

###### Name Plugin: Breadcrumb NavXT
###### Link Download: https://wordpress.org/plugins/breadcrumb-navxt/
```bash
Breadcrumb NavXT, the successor to the popular WordPress plugin Breadcrumb Navigation XT, was written from the ground up to be better than its ancestor. 
This plugin generates locational breadcrumb trails for your WordPress powered blog or website. 
These breadcrumb trails are highly customizable to suit the needs of just about any website running WordPress. 
The Administrative interface makes setting options easy, while a direct class access is available for theme developers and more adventurous users.
```

###### Name Plugin: Contact Form 7
###### Link Download: https://wordpress.org/plugins/contact-form-7/
```bash
Contact Form 7 can manage multiple contact forms, plus you can customize the form and the mail contents flexibly with simple markup. 
The form supports Ajax-powered submitting, CAPTCHA, Akismet spam filtering and so on.
```

###### Name Plugin: Contact Form 7 add confirm
###### Link Download: https://wordpress.org/plugins/contact-form-7-add-confirm/
```bash
“Contact Form 7 add confirm” can add confirm step to “Contact Form 7”.
“Contact Form 7 add confirm”に確認画面を追加するプラグインです。
This plug-in is not related to the developer of the “Contact Form 7”.
このプラグインの作者はContact Form 7プラグイン開発元とは関係ありません、ご注意下さい。
```

###### Name Plugin: Contact Form 7 – Conditional Fields
###### Link Download: https://wordpress.org/plugins/cf7-conditional-fields/
```bash
This plugin adds conditional logic to Contact Form 7.

If you edit your CF7 form, you will see an additional tag called “Conditional fields Group”.
Everything you put between the start and end tag will be hidden by default.
After you have added the field group(s), click Save and go to the “Conditional fields” tab to create one or more conditions that will make the group(s) appear.
```

###### Name Plugin: Classic Editor
###### Link Download: https://wordpress.org/plugins/classic-editor/
```bash
Classic Editor is an official plugin maintained by the WordPress team that restores the previous (“classic”) WordPress editor and the “Edit Post” screen. 
It makes it possible to use plugins that extend that screen, add old-style meta boxes, or otherwise depend on the previous editor.
Classic Editor is an official WordPress plugin, and will be fully supported and maintained until at least 2022, or as long as is necessary.
```

###### Name Plugin: Compress JPEG & PNG images
###### Link Download: https://wordpress.org/plugins/tiny-compress-images/
```bash
Make your website faster by optimizing your JPEG and PNG images.
This plugin automatically optimizes all your images by integrating with the popular image compression services TinyJPG and TinyPNG.
```

###### Name Plugin: Custom Post Type UI
###### Link Download: https://wordpress.org/plugins/custom-post-type-ui/
```bash
Custom Post Type UI provides an easy to use interface for registering and managing custom post types and taxonomies for your website.
```

###### Name Plugin: Simple Custom Post Order
###### Link Download: https://wordpress.org/plugins/simple-custom-post-order/
```bash
Order posts(posts, any custom post types) using a Drag and Drop Sortable JavaScript. Configuration is unnecessary.
You can do directly on default WordPress administration.
Excluding custom query which uses order or orderby parameters, in get_posts or query_posts and so on.
```

###### Name Plugin: Duplicate Post
###### Link Download: https://wordpress.org/plugins/duplicate-post/
```bash
This plugin allows users to clone posts of any type, or copy them to new drafts for further editing.
If you find this useful, please consider donating whatever sum you choose, even just 10 cents.
Just a few cents from every user would help me develop the plugin and improve support.
```

###### Name Plugin: Loco Translate
###### Link Download: https://wordpress.org/plugins/loco-translate/
```bash
Loco Translate provides in-browser editing of WordPress translation files.

It also provides localization tools for developers, such as extracting strings and generating templates.
```

###### Name Plugin: Parent Category Toggler
###### Link Download: https://wordpress.org/plugins/parent-category-toggler/
```bash
Automatically toggle the parent categories when a sub category is selected.
```

###### Name Plugin: Regenerate Thumbnails
###### Link Download: https://wordpress.org/plugins/regenerate-thumbnails/
```bash
Regenerate Thumbnails allows you to regenerate all thumbnail sizes for one or more images that have been uploaded to your Media Library.

This is useful for situations such as:

A new thumbnail size has been added and you want past uploads to have a thumbnail in that size.
You’ve changed the dimensions of an existing thumbnail size, for example via Settings → Media.
You’ve switched to a new WordPress theme that uses featured images of a different size.
It also offers the ability to delete old, unused thumbnails in order to free up server space.
```

###### Name Plugin: Radio Buttons for Taxonomies
###### Link Download: https://wordpress.org/plugins/radio-buttons-for-taxonomies/
```bash
Replace the default taxonomy boxes with a custom metabox that uses radio buttons… effectively limiting each post to a single term in that taxonomy.

A plugin options page allows the user can select which taxonomies she’d like to switch to using the custom radio-button style metabox.
```

###### Name Plugin: WP Super Cache
###### Link Download: https://wordpress.org/plugins/wp-super-cache/
```bash
This plugin generates static html files from your dynamic WordPress blog. 
After a html file is generated your webserver will serve that file instead of processing the comparatively heavier and more expensive WordPress PHP scripts.
```

###### Name Plugin: WP-Optimize – Clean, Compress, Cache
###### Link Download: https://wordpress.org/plugins/wp-optimize/
```bash
WP-Optimize is a revolutionary, all-in-one plugin that cleans your database, compresses your images and caches your site. This simple, 
popular and highly effective tool has everything you need to keep your website fast and thoroughly optimized!
```

###### Name Plugin: WP Mail SMTP by WPForms
###### Link Download: https://vi.wordpress.org/plugins/wp-mail-smtp/
```bash
Having problems with your WordPress site not sending emails? You’re not alone. Over 1 million websites use WP Mail SMTP to send their emails reliably.
Our goal is to make email deliverability easy and reliable. We want to ensure your emails reach the inbox.
WP Mail SMTP fixes your email deliverability by reconfiguring WordPress to use a proper SMTP provider when sending emails.
```

###### Name Plugin: WP Multilang
###### Link Download: https://wordpress.org/plugins/wp-multilang/
```bash
WP Multilang is a multilingual plugin for WordPress.
Translations of post types, taxonomies, meta fields, options, text fields in miltimedia files, menus, titles and text fields in widgets.
```