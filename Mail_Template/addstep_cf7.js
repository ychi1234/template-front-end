$(document).ready(function() {
    
    var name_step1 = 'Step1';
    var name_step2 = 'Step2';    
    if ($('.wpcf7 .wpcf7c-elm-step1').length + $('.wpcf7 .wpcf7c-elm-step2').length > 0) {
        setting_step_class(name_step1,name_step2);
    }

    function setting_step_class(name_step1,name_step2) {
        addClassStep(name_step1);
        document.addEventListener('wpcf7submit', function(event) {
            if ($('.wpcf7c-elm-step1').hasClass('wpcf7c-force-hide')) {
                addClassStep(name_step2);
            } else {
                addClassStep(name_step1);
            }
        }, false);
        $('.wpcf7 input[type=button]').click(function() {
            if ($('.wpcf7c-elm-step1').hasClass('wpcf7c-force-hide')) {
                addClassStep(name_step2);
            } else {
                addClassStep(name_step1);
            }
        });
    }

    function addClassStep(step_name) {
        var divcf7 = $('.wpcf7');
        divcf7.removeClass(name_step1).removeClass(name_step2);
        divcf7.addClass(step_name);
        console.log('****_ add class |Cf7classStep| for auto add class step form _****');
        $('.Cf7classStep').removeClass(name_step1).removeClass(name_step2).addClass(step_name);
    }


});
