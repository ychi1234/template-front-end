/***************************************************************************
*
* SCRIPT JS
*
***************************************************************************/

$(document).ready(function(){
    
    // REMOVE PRESS E CHARACTER
    document.querySelector("input[type=number]").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });


    //SCROLL TO TOP 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 40) {
            $('.scrollToTop').fadeIn(400);
        } else {
            $('.scrollToTop').fadeOut(400);
        }
    });


    //SCROLL ANCHOR
    $('.anchor a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    // ACTIVE MENU
    if ($(window).width() <= 768) {
        $('.hamburger').click(function(event) {
            $(this).toggleClass('toggle');
            $('#header .wrapMenu').stop().slideToggle();
            $('body').toggleClass('fixed');
        });
    }

    // SP
    function heightFix() {
        var hf = $('.headerSP').outerHeight();
        var menuh = $(window).height() - hf;
        $('#content').css('margin-top', hf);
        $('#header .wrapMenu').css('height', menuh);
    }

    function fullheight(){
        var tf = $('#content .wrapContent .pageTitle').outerHeight() + 15;
        var ch = $(window).height() - tf;
        
        // if($('body').hasClass('fullheight')){
        //     $('#content .wrapContent .wrapArea').css({
        //         'height': ch,
        //         'overflow': 'hidden'
        //     });
        // }

        // CONTACT PAGE
        if($('.contactPage').length){
            var contentH = $('.contactPage .contactContent').height();
            var pd = (ch - contentH)/2;
            $('.contactPage .contactContent').css({
                'padding-top': pd,
                'padding-bottom': pd
            });
        } 

        // MESSAGE PAGE
        if($('.messagePage').length){
            var contentH = $('.messagePage .messContent').height();
            var pd = (ch - contentH)/2;
            $('.messagePage .messContent').css({
                'padding-top': pd,
                'padding-bottom': pd
            });
        }        

        // CONCEPT PAGE
        if($('.conceptPage').length){
            var contentH = $('.conceptPage .conceptContent').height(); 
            if( ch < 700 ){
                ch = 700
                var pd = (ch - contentH)/2;
                $('.conceptPage .conceptContent').css({
                    'padding-top': pd,
                    'padding-bottom': pd
                });
            }          
        }

        // COMPANY PAGE
        if($('.companyPage').length){
            var contentH = $('.companyPage .companyContent').height(); 
            var pd = (ch - contentH)/2;
            $('.companyPage .companyContent').css({
                'padding-top': pd,
                'padding-bottom': pd
            });     
        }
    }
    
    $(window).load(function(){
        if ($(window).width() >= 769) {
            fullheight();
        }
    });

    if ($(window).width() <= 768) {
        heightFix();    
    }else{
        $('#content').removeAttr('style');
    }
    
    $(window).resize(function() {
        if ($(window).width() <= 768) {
            heightFix();
        }else{
            $('#content').removeAttr('style');
        }
        if ($(window).width() >= 769) {
            fullheight();
        }
    });
    
    // FANCYBOX
    if($(".fancybox").length) {
        $(".fancybox").fancybox({
            mouseWheel: false,
            loop: true,
            clickContent: false,
        });
    }
    $('#header').click(function() {
        $.fancybox.close();
    });
    $('.mainTitle').click(function() {
        $.fancybox.close();
    });
    $('.pageTitle').click(function() {
        $.fancybox.close();
    });


    function SyncHeightItem( itemElements, itemElementsWrap ) {
        if($(window).width() > 769) {
            var eduarray = [];
            $('.mainContent .collectionList li').each(function() {
                var s = $(this).find(itemElementsWrap).outerHeight();
                eduarray.push(s);

            });
            var max_num = Math.max.apply(Math,eduarray);
            $('.mainContent .collectionList li '+itemElements).css('height', max_num);
        }
        else{
            $('.mainContent .collectionList li '+itemElements).removeAttr('style');
        }
    }
    $(window).resize(function(){
        SyncHeightItem('.photoInfo', '.photoInfoWrap');
        SyncHeightItem('.textDetail', '.textDetailWrap');
    });
    $(window).load(function(){
        SyncHeightItem('.photoInfo', '.photoInfoWrap');
        SyncHeightItem('.textDetail', '.textDetailWrap');
    });



    // VALIDATE
    if($('#form_contact').length){
        $("#form_contact").validate({
            lang: 'en',
            ignore:'',
            rules: {
                yourname: "required",
                message: "required",
                your_mail: {
                    required: true,
                    email: true
                }, 
            }
        });
    }
    
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function(m, key, value) {
                vars[key] = value;
            });
        return vars;
    }

    var fStep = getUrlVars()["step"];
    if (fStep && fStep == "finish") {
        $('#form_contact').css('display', 'none');
        $('.wrapform').append('<p class="success">SEND MAIL COMPLETED.<br>THANK YOU VERY MUCH.</p>')
    }



    // scroll to top animation

    $(window).scroll(function() {
        if($('.scrollToTop').length){
            if ($(window).scrollTop() + $(window).height() > ($("#copy").offset().top - $("#copy").height())) {
                $('.scrollToTop').addClass('active');
            } else {
                $('.scrollToTop').removeClass('active');
            }
        }
    });

});